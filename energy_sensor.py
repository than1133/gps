from gpiozero import MCP3008


solar_volt_val = MCP3008(0)
solar_current_val = MCP3008(1)
piezo_volt_val = MCP3008(2)
piezo_current_val = MCP3008(3)

def convert_volt(val):
    v = (val * 3.3)
    v = round(v, 2)
    return v

def convert_current(val):
    pass

