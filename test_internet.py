import urllib3
import time

def check_internet():
    http = urllib3.PoolManager()
    r = http.request('GET', 'http://google.com')
    if r.status == 200:
        return 1
    else:
        return 0

def main():
    while True:
        print(check_internet())
        time.sleep(1)

if __name__ == "__main__":
    main()
