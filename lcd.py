import I2C_LCD_driver
from time import *

mylcd = I2C_LCD_driver.lcd()

mylcd.lcd_display_string("Hello World!", 1)
sleep(1)
mylcd.lcd_clear()

i = input('Enter \'on\' or \'off\': ')
if i == "on":
    mylcd.backlight(1)
else:
    mylcd.backlight(0)

