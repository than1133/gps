from gpiozero import MCP3008
from time import sleep

c0 = MCP3008(0)
c1 = MCP3008(1)

def mcp_convert(val):
    v = val * 3.3
    v = round(v, 2)
    return v

while 1:
    val1 = mcp_convert(c0.value)
    val2 = mcp_convert(c1.value)
    result = val1*val2
    print(str(round(result, 2))+"W")
    sleep(1)
	
