import math

def degree_to_radians(degree):
    return degree * math.pi / 180

def cal_distance(p1, p2):
    print(p1, p2)
    earth_radius_km = 6371

    dlat = degree_to_radians(p2[0]-p1[0])
    dlon = degree_to_radians(p2[1]-p1[1])

    lat1 = degree_to_radians(p1[0])
    lat2 = degree_to_radians(p2[0])

    a = (math.sin(dlat/2)
        * math.sin(dlat/2)
        + math.sin(dlon/2)
        * math.sin(dlon/2)
        * math.cos(lat1)
        * math.cos(lat2))
    c = 2 * math.atan2(math.sqrt(a), math.sqrt(1-a))

    return earth_radius_km * c

def km_to_m(km):
    return km / 0.0010000
