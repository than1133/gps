import csv

def save_distance(idd, mn_pos, rnd, cur_pos, dis):
    with open('data.csv', 'a', newline='') as csvfile:
        spamwriter = csv.writer(csvfile
                , delimiter=' '
                , quotechar='|'
                , quoting=csv.QUOTE_MINIMAL
                )
        spamwriter.writerow([str(idd), str(mn_pos), str(rnd), str(cur_pos), str(dis)])
