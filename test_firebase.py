import os 
import string_date as sd 
import firebase_sender as fs
    
def main():
    print("start test firebase")
    mission_name = str(sd.get_date())
    print("mission name is:"+str(mission_name))
    fs.initialize_mission(mission_name)
    fs.push_data(mission_name, fs.data_test) 
    fs.get_data_from_mission(mission_name)
    fs.get_mis_com(mission_name)
    fs.set_mis_com(mission_name, 'True')
    fs.get_sos(mission_name)
    fs.set_sos(mission_name, 'True')
    alert = fs.get_alert(mission_name)
    print(alert[0])
    print(alert[1])
    print("wait alert change...")
    while True:
        alert_if_changed = fs.get_alert_if_change(mission_name, alert[1])
        if alert_if_changed[0] == True:
            print("alert listen: OK")
            break


if __name__ == "__main__":
    main()
