import os
import time
import urllib3
import multiprocessing as mp
import random
import I2C_LCD_driver
import gpiozero
import threading
import gps_cal
import save_csv
import firebase_sender as fs
import string_date as sd
import firebase_admin
import get_mission_name
from git import Repo
from firebase_admin import credentials
from firebase_admin import db
from gps3 import agps3
from gpiozero import MCP3008

mylcd = I2C_LCD_driver.lcd()
mission_name = get_mission_name.get_name()
##
#mis_com_btn = gpiozero.Button(13)
#sos_btn = gpiozero.Button(19)
#scr_btn = gpiozero.Button(26)

# firebase sender process
def firebase_sender(wait_time, gps_lat, gps_lon, solar_volt, solar_current, piezo_volt):
    print('firebase sender start')
    while True:
        data = {
                'lat': gps_lat.value, 
                'lon': gps_lon.value, 
                'solar_voltage': solar_volt.value,
                'solar_current': solar_current.value,
                'piezo_voltage': piezo_volt.value,
                'time': sd.get_date()
                }
        fs.push_data(mission_name, data)
        time.sleep(wait_time)

# internet checking
def internet_checking():
    http = urllib3.PoolManager()
    r = http.request('GET', 'http://google.com')
    if r.status == 200:
        return 1
    else:
        print("can't connect internet")
        return 0
        

# internet checking process
def internet_check(i):
    print('internet process start')
    while True:
        i.value = internet_checking()
        time.sleep(5)

# GPS watching process
def gps_watching(updating, page, lat, lon, sat):
    print('gps process start')
    gps_socket = agps3.GPSDSocket()
    data_stream = agps3.DataStream()
    gps_socket.connect()
    gps_socket.watch()
    for new_data in gps_socket:
        if new_data:
            data_stream.unpack(new_data)
            
            if data_stream.satellites != 'n/a':
                sat.value = len(data_stream.satellites)
            else:
                sat.value = 0
            
            if data_stream.lat == 'n/a':
                lat.value = 0.0
            else:
                lat.value = data_stream.lat

            if data_stream.lon == 'n/a':
                lon.value = 0.0
            else:
                lon.value = data_stream.lon
        time.sleep(5)
        if page.value == 1:
            updating_state(updating)
        
            
# display log1
def print_log1(inet_conn, _arduino, send_counter, lat, lon, satellites):
    while True:          
       
        new_data = _arduino.get()
        
        data_pack = {
            'sos': 0,
            'mis_com': 0,
            'screen': 0,
            'vibrate': {
                'voltage': 0,
                'current': 0
            },
            'solar': {
                'voltage': 0,
                'current': 0
            },
            'firebase_counter': send_counter.value,
            'gps_data': {
                'lat': lat.value,
                'lon': lon.value,
                'satellites': satellites.value
            },
            'internet_connection': inet_conn.value
        }
        
        if new_data != False:
            data_pack['sos'] = new_data['sos']
            data_pack['mis_com'] = new_data['mis_com']
            data_pack['screen'] = new_data['screen']
            data_pack['vibrate']['voltage'] = new_data['vibrate_voltage']
            data_pack['vibrate']['current'] = new_data['vibrate_current']
            data_pack['solar']['voltage'] = new_data['solar_voltage']
            data_pack['solar']['current'] = new_data['solar_current']
            
        vibrate = data_pack['vibrate']
        solar = data_pack['solar']
        gps = data_pack['gps_data']
        os.system('clear')
        
        print("sos pressed: " + data_pack['sos'])
        print("mission complete pressed: " + data_pack['mis_com'])
        print("screen pressed: " + data_pack['screen'])
        print("vibrate energy: " + vibrate['voltage'] + "v, " + vibrate['current'] + "a")
        print("solar energy: " + solar['voltage'] + "v, " + solar['current'] + "a")
        print("firebase send data time left: " + data_pack['firebase_counter'] + "s")
        print("internet connection: " + data_pack['internet_connection'])
        print("gps connection: lat " + gps['lat'] + ", lon " +  gps['lon'])
        print("satellites: "+ gps['satellites'])
        time.sleep(0.15)

# use for update state of ilcd display
def updating_state(state):
    state.value = 1
    time.sleep(0.1)
    state.value = 0

# used for display template of lcd 16x2
def display_model(string0="", string1=""):
    mylcd.lcd_clear()
    mylcd.lcd_display_string(str(string0), 1)
    mylcd.lcd_display_string(str(string1), 2)

def screen_saver():
    while True:
        pass
        #time.sleep(10)
        #mylcd.backlight(0)

def generate_line(val, lat):
    return str(val) + (' ' * (15-len(str(val)))) + chr(lat)

def display_gps(gps_lat, gps_lon, m, s):
    l1 = generate_line(gps_lat, m)
    l2 = generate_line(gps_lon, s)
    display_model(l1, l2)

def display_solar_piezo(m, s, solar_volt, solar_current, piezo_volt):
    s1 = "Battey"
    s2 = str(solar_volt)
    l1 = generate_line(s1, m)
    l2 = generate_line(s2, s)
    display_model(l1, l2)

def display_ess(m, s):
    s1 = "%ESS 98"
    l1 = generate_line(s1, m)
    l2 = generate_line('', s)
    display_model(l1, l2)

def display_view(
        update_state, 
        page, 
        gps_lat, 
        gps_lon, 
        solar_volt, 
        solar_current, 
        piezo_volt, 
        m, 
        s):
    display_gps(gps_lat.value, gps_lon.value, m.value, s.value)
    while True:
        if update_state.value == 1:
            if page.value == 1:
                display_gps(gps_lat.value, gps_lon.value, m.value, s.value)
            elif page.value == 2:
                display_solar_piezo(m.value, s.value, solar_volt.value, solar_current.value, piezo_volt.value)
            else:
                display_ess(m.value, s.value)

def mis_com_check_letter(is_mis_com):
    if is_mis_com == 32:
        fs.set_mis_com(mission_name, 'True')
        return 77
    else:
        fs.set_mis_com(mission_name, 'False')
        return 32

def mis_com_event(update_state, is_mis_com):
    print('mission complete process start')
    mis_com_gpio = gpiozero.Button(13)
    is_mis_com.value = 77 if fs.get_mis_com(mission_name) == 'True' else 32
    trigger = False
    while True:
        if mis_com_gpio.is_pressed:
            if trigger == False:
                # event
                is_mis_com.value = mis_com_check_letter(is_mis_com.value)
                updating_state(update_state)
                print(chr(is_mis_com.value))
                time.sleep(0.5)
            trigger = True
        else:
            trigger = False

def sos_check_letter(is_sos):
    if is_sos == 32:
        fs.set_sos(mission_name, 'True')
        return 83
    else:
        fs.set_sos(mission_name, 'False')
        return 32

def sos_event(update_state, is_sos):
    print('sos event process start')
    sos_gpio = gpiozero.Button(19)
    is_sos.value = 83 if fs.get_sos(mission_name) == 'True' else 32
    trigger = False
    while True:
        if sos_gpio.is_pressed:
            if trigger == False:
                # event
                is_sos.value = sos_check_letter(is_sos.value) 
                updating_state(update_state) 
                print(chr(is_sos.value))
                time.sleep(0.5)
            trigger = True
        else:
            trigger = False
    
def change_page_value(page):
    if page >= 3:
        return 1
    return page + 1

page_text = ["gps", "power sensor", "ess"]

def change_page_event(update_state, page):
    print('change page event process start')
    change_page_gpio = gpiozero.Button(26)
    trigger = False
    while True:
        if change_page_gpio.is_pressed:
            if trigger == False:
                # event
                #updating_state(update_state)
                page.value = change_page_value(page.value)  
                print("in "+page_text[page.value-1]+" page")
                updating_state(update_state)
                time.sleep(0.5)
            trigger = True
        else:
            trigger = False

def check_bool_text(b):
    if b == 'True':
        return True
    else:
        return False

def set_led(led_state):
    print('led setter process start')
    alert = fs.get_alert(mission_name)
    led_state.value = int(check_bool_text(alert[0]))
    etag = alert[1]
    while True:
        alert_changed = fs.get_alert_if_change(mission_name, etag)
        if alert_changed[0] == True:
            led_state.value = int(check_bool_text(alert_changed[1]))
            etag = alert_changed[2]
        time.sleep(1)

def led_control(led_state):
    print('led controll process start')
    led = gpiozero.LED(6)
    while True:
        if led_state.value == 1:
            led.on()
            time.sleep(0.5)
            led.off()
            time.sleep(0.5)
        else:
            led.off()

solar_current_pin = MCP3008(0)
solar_volt_pin = MCP3008(1)
piezo_volt_pin = MCP3008(2)

def mcp_converter(val):
    v = val*(3.3/1024)*5
    v = round(v, 3)
    return v*1000

def mcp_converter_current(val):
    return val*132.452941176

def power_sensor(updating, page, solar_volt, solar_current, piezo_volt):
    print("power sensor start")
    while True:
        sv = mcp_converter(solar_volt_pin.value)
        sc = mcp_converter(solar_current_pin.value)
        solar_volt.value = sv
        solar_current.value = sc

        pv = mcp_converter_current(piezo_volt_pin.value)
        piezo_volt.value = pv
        time.sleep(1)
        if page.value == 2:
            updating_state(updating)

def main():
    inet_stat_val = mp.Value('i', 0)

    power_solar_volt = mp.Value('d', 0.0)
    power_solar_current = mp.Value('d', 0.0)
    power_piezo_volt = mp.Value('d', 0.0)

    gps_lat = mp.Value('d', 0.0)
    gps_lon = mp.Value('d', 0.0)
    gps_sat = mp.Value('i', 0)
    
    mis_com_btn = mp.Value('i', 0)
    sos_btn = mp.Value('i', 0)
    scr_btn = mp.Value('i', 0)

    page = mp.Value('i', 1)
    
    # M = 77
    # S = 83
    # ' ' = 32
    is_mis_com = mp.Value('i', 32)
    is_sos = mp.Value('i', 32) 
    update_state = mp.Value('i', 0)
    led_state = mp.Value('i', 0)
    
    display_model("starting", "program")
    time.sleep(1)
    display_model("checking", "internet")
    inet_stat_val.value = internet_checking()
    if inet_stat_val.value == 1:
        display_model("internet ok")
    else:
        display_model("can't connet", "internet")
    time.sleep(1)
    display_model("checking system", "update")
    repo = Repo("/home/pi/gps/")
    git = repo.git
    git.fetch()
    c = git.checkout("master")
    cc = c.split("pull")
    #status = os.system("git rev-list HEAD...origin/master --count")
    if len(cc) == 1:
        print("system is update")
        display_model("system is", "updated")
        time.sleep(1)
    else:
        print("system isn't update")
        display_model("system isn't", "update")
        time.sleep(1)
        print("updating system")
        display_model("updating", "system")
        time.sleep(1)
        repo.git.pull()
        print("update finish")
        display_model("update finish")
        time.sleep(1)
        print("restarting system")
        display_model("restarting", "system")
        time.sleep(1)
        os.system("sudo reboot")

    print("starting process")
    display_model("starting", "process")
    time.sleep(1)
    # define process 
    internet_checking_process = mp.Process(target=internet_check, args=(inet_stat_val,))
    firebase_sending_process = mp.Process(
            target=firebase_sender,
            args=(
                10, 
                gps_lat, 
                gps_lon,
                power_solar_volt, 
                power_solar_current, 
                power_piezo_volt,)
            )  
    gps_watching_process = mp.Process(
            target=gps_watching, 
            args=(
                update_state, 
                page, 
                gps_lat, 
                gps_lon, 
                gps_sat,)
            )
    mis_com_event_process = mp.Process(target=mis_com_event, args=(update_state, is_mis_com,))
    sos_event_process = mp.Process(target=sos_event, args=(update_state, is_sos,))
    change_page_event_process = mp.Process(target=change_page_event, args=(update_state, page,))
    display_view_process = mp.Process(
            target=display_view,
            args=(
                update_state,
                page, gps_lat,
                gps_lon, 
                power_solar_volt, 
                power_solar_current,
                power_piezo_volt, 
                is_mis_com, 
                is_sos,)
            )
    screen_saver_process = mp.Process(target=screen_saver)
    led_setter_process = mp.Process(target=set_led, args=(led_state,))
    led_control_process = mp.Process(target=led_control, args=(led_state,))
    power_sensor_process = mp.Process(
            target=power_sensor, 
            args=(
                update_state, 
                page, 
                power_solar_volt, 
                power_solar_current, 
                power_piezo_volt,)
            )

    # start process
    internet_checking_process.start()
    firebase_sending_process.start()    
    gps_watching_process.start()
    mis_com_event_process.start()
    sos_event_process.start()
    change_page_event_process.start()
    display_view_process.start()
    screen_saver_process.start()
    led_setter_process.start()
    led_control_process.start()
    power_sensor_process.start()


if __name__ == '__main__':
    main()
    
## Test GPS section
#
#position = [[13.847331, 100.572067],]
#
#def next_page(p):
#    if p == 4:
#        return 1
#    return p+1
#
#def display_only_gps(lat, lon):
#    mylcd.lcd_clear()
#    mylcd.lcd_display_string(str(lat), 1)
#    mylcd.lcd_display_string(str(lon), 2)
#
#def display_local_round(l, r):
#    mylcd.lcd_clear()
#    mylcd.lcd_display_string("local: " + str(l), 1)
#    mylcd.lcd_display_string("round: "+ str(r), 2)
#
#def display_press_start():
#    mylcd.lcd_clear()
#    mylcd.lcd_display_string("press start", 1)
#
#def display_round_save(r):
#    mylcd.lcd_clear()
#    mylcd.lcd_display_string("saving", 1)
#    mylcd.lcd_display_string("round: "+str(r),2)
#
#def change_local(loc, local_max):
#    if loc >= local_max:
#        return 1
#    return loc + 1
#
#def change_round(rnd, rnd_max):
#    if rnd >= rnd_max:
#        return 1
#    return rnd + 1
#
#def save(local, pos, rnd, gps_lat, gps_lon, gps_sat):
#    new_main_pos_str = str(pos[local-1][0]) + ", " + str(pos[local-1][1])
#    current_pos_str = (gps_lat, gps_lon)
#
#    new_main_pos = (pos[local-1][0], pos[local-1][1])
#    current_pos = (gps_lat, gps_lon)
#    dis = gps_cal.cal_distance(new_main_pos, current_pos)
#    dis = gps_cal.km_to_m(dis)
#
#    save_csv.save_distance(local, new_main_pos_str, rnd, current_pos_str, dis)
#    #display_only_gps(gps_lat, gps_lon)
#
#def gps_watching1(gps_lat, gps_lon, gps_sat):
#    gps_socket = agps3.GPSDSocket()
#    data_stream = agps3.DataStream()
#    gps_socket.connect()
#    gps_socket.watch()
#    for new_data in gps_socket:
#        if new_data:
#            data_stream.unpack(new_data)
#            
#            if data_stream.satellites != 'n/a':
#                gps_sat.value = len(data_stream.satellites)
#            else:
#                gps_sat.value = 0
#            
#            if data_stream.lat == 'n/a':
#                gps_lat.value = 0.0
#            else:
#                gps_lat.value = data_stream.lat
#
#            if data_stream.lon == 'n/a':
#                gps_lon.value = 0.0
#            else:
#                gps_lon.value = data_stream.lon
#    
#        time.sleep(1)
# 
#def screen_saver():
#    while True:
#        time.sleep(5)
#        mylcd.backlight(0)
#
## save loop
#def scr_pressed(pages, gps_lat, gps_lon, rounding, save_state):
#    btn = gpiozero.Button(26)
#    trigger = False
#    while True:
#        if btn.is_pressed:
#            if trigger == False:
#                if pages.value == 1:
#                    if save_state.value == 0:
#                        save_state.value = 1
#                    else:
#                        save_state.value = 0
#                time.sleep(1)
#            trigger = True
#        else:
#            trigger = False
#
## change page 
#def sos_pressed(pages, gps_lat, gps_lon, rounding, save_state):
#    btn = gpiozero.Button(19)
#    trigger = False
#    while True:
#        if btn.is_pressed:
#            if trigger == False:
#                if save_state.value == 0:
#                    pages.value += 1
#                    if pages.value > 1:
#                        pages.value = 0
#                time.sleep(1)
#            trigger = True
#        else:
#            trigger = False
#
#def mis_com_pressed(localing, rounding):
#    btn = gpiozero.Button(13)
#    trigger = False
#    while True:
#        if btn.is_pressed:
#            if trigger == False:
#                round_max = 100
#                rounding.value = change_round(rounding.value, round_max)
#                display_local_round(localing.value, rounding.value)
#                time.sleep(1)
#
#            trigger = True
#        else:
#            trigger = False
#
#def display_updating(pages, gps_lat, gps_lon, rounding, save_state):
#    while True:
#        # display only gps
#        if pages.value == 0:
#            display_only_gps(gps_lat.value, gps_lon.value)
#        if pages.value == 1:
#            if save_state.value == 0:
#                print(gps_lat.value, gps_lon.value)
#                rounding.value = 0
#                display_press_start()
#            if save_state.value == 1:
#                while(rounding.value <= 100):
#                    display_round_save(rounding.value)
#                    save(0, position, rounding.value, gps_lat.value, gps_lon.value, gps_sat.value)
#                    rounding.value += 1
#                    time.sleep(1)
#
#        time.sleep(1)
#
#if __name__ == "__main__":
#    
#    pages = mp.Value('i', 0)
#    gps_lat = mp.Value('d', 0.0)
#    gps_lon = mp.Value('d', 0.0)
#    gps_sat = mp.Value('i', 0)
#    localing = mp.Value('i', 1)
#    rounding = mp.Value('i', 1)
#    save_state = mp.Value('i', 0)
#    
#    gps_watching_process = mp.Process(target=gps_watching1, args=(gps_lat, gps_lon, gps_sat,))
#    #screen_saver_process = mp.Process(target=screen_saver)
#    
#    # save here
#    # this variable stored Yello button 
#    scr_process = mp.Process(target=scr_pressed, args=(pages, gps_lat, gps_lon, rounding, save_state,))
#    
#    # change page here 
#    # this variable stored Red button
#    sos_process = threading.Thread(target=sos_pressed, args=(pages, gps_lat, gps_lon, rounding, save_state,))
#    
#    # this variable stored Green button
#    #mis_com_process = threading.Thread(target=mis_com_pressed, args=(localing, rounding,))
#    
#    gps_watching_process.start()
#    #screen_saver_process.start()
#    scr_process.start()
#    sos_process.start()
#    #mis_com_process.start()
#    
#    display_process = mp.Process(target=display_updating, args=(pages, gps_lat, gps_lon, rounding, save_state,))
#    display_process.start()
#
#
