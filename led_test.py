from gpiozero import LED
from signal import pause
from time import sleep

led = LED(6)

alert = True

while True:
    if alert == True:
        led.blink()
        alert = False

