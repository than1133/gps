import os
from gpiozero import MCP3008
from time import sleep

c0 = MCP3008(0)
c1 = MCP3008(1)
c2 = MCP3008(2)

def mcp_convert_current(val):
    return val*132.452941176

def mcp_convert_volt(val):
    v = val*(3.3/1024)*5
    v = round(v, 3)
    return v*1000

def mpc_convert(val):
    pass

def print_chanel_value(chanel, value):
    print("chanel "+str(chanel)+": "+str(value))

while 1:
    os.system('clear')
    val0 = mcp_convert_current(c0.value)
    val1 = mcp_convert_volt(c1.value)
    val2 = mcp_convert_volt(c2.value)
    print_chanel_value(0, val0)
    print_chanel_value(1, val1)
    print_chanel_value(2, val2)
    #result = val1*val2
    #print(str(round(result, 2))+"W")
    sleep(1)
	
