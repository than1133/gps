import firebase_admin
import sys
from pprint import pprint
from firebase_admin import credentials
from firebase_admin import db

firebase_url = 'https://gpstest-189609.firebaseio.com'
cert = '/home/pi/gps/gpsproject02-f32b5-firebase-adminsdk-eqxs9-d155ecf20b.json'
cred = credentials.Certificate(cert)
default_app = firebase_admin.initialize_app(cred, {'databaseURL': 'https://gpsproject02-f32b5.firebaseio.com'})

mis_name = 'AT001'
data_init = {
    "data":{
        "test": { 
            'lat': 0,
            'lon': 0,
            'solar_current': 0,
            'solar_voltage': 0,
            'piezo_voltage': 0,
            'time': 0
            }
        },
        "signal": {
            "mis_com": "False",
            "sos": "False",
            "alert": "False"
            }
        }

data_test = {
        'lat': 0,
        'lon': 0,
        'solar_current': 0,
        'solar_voltage': 0,
        'piezo_voltage': 0,
        'time': 0
        }

signal_false = {
        "signal": {
                "mis_com": "False",
                "sos": "False",
                "alert": "False"
                }
        }

database = db.reference()
missions = database.child('missions')

def print_except(text):
    print(text+str(sys.exc_info()[0]))

def initialize_mission(mission_name):
    try:
        missions.child(mission_name).set(data_init)
        print("initialize mission: OK")
    except:
        print_except("initialize mission: ")

def get_mission(mission_name):
    mission = missions.child(mission_name)
    return mission

def get_data(mission_name):
    data = get_mission(mission_name).child('data')
    return data

def get_signal(mission_name):
    signal = get_mission(mission_name).child('signal')
    return signal

def push_data(mission_name, data):
    try:
        get_data(mission_name).push(data)
        print("push data to mission: OK")
    except:
        print_except("push data to mission: ")

def get_data_from_mission(mission_name):
    try:
        data, etag = get_data(mission_name).get(etag=True)
        pprint((data,etag))
        print("get data from mission: OK")
    except:
        print_except("get data from mission: ")


def get_mis_com(mission_name):
    try:
        mis_com = get_signal(mission_name).child('mis_com').get()
        print("get mis com: OK")
        return mis_com
    except:
        print_except("get mis com:")

def set_mis_com(mission_name, value):
    try:
        get_signal(mission_name).update({'mis_com': value})
        print("set mis com: OK")
    except:
        print_except("set mis com: ")

def get_sos(mission_name):
    try:
        sos = get_signal(mission_name).child('sos').get()
        print("get sos: OK")
        return sos
    except:
        print_except("get sos: ")

def set_sos(mission_name, value):
    try:
        get_signal(mission_name).update({'sos': value})
        print("set sos: OK")
    except:
        print_except("set sos: ")

def get_alert(mission_name):
    try:
        alert, etag = get_signal(mission_name).child('alert').get(etag=True)
        print("get alert: OK")
        return (alert, etag)
    except:
        print_except("get alert: ")

def get_alert_if_change(mission_name, etag):
    alert = get_signal(mission_name).child('alert').get_if_changed(etag)
    return alert

#if __name__ == '__main__':
#   print(get_mission_state(mis_name, 'sos')) 

