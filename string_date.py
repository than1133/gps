import datetime

def plus_minuse(txt):
    txt += '-'
    return txt

def plus_colon(txt):
    txt += ':'
    return txt

def plus_comma(txt):
    txt += ","
    return txt

def get_date():
    now = datetime.datetime.now()
    stime = str(now.year)
    stime = plus_minuse(stime)
    stime += str(now.month)
    stime = plus_minuse(stime)
    stime += str(now.day)
    stime = plus_comma(stime)
    stime += str(now.hour)
    stime = plus_colon(stime)
    stime += str(now.minute)
    stime = plus_colon(stime)
    stime += str(now.second)
    return stime
