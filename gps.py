from gps3 import agps3

def gps_watching():
    gps_socket = agps3.GPSDSocket()
    data_stream = agps3.DataStream()
    gps_socket.connect()
    gps_socket.watch()
    for new_data in gps_socket:
        if new_data:
            data_stream.unpack(new_data)

            sat = data_stream.satellites
            lat = data_stream.lat
            lon = data_stream.lon
            
            print(str(lat)+', '+str(lon)+', '+str(sat))

def get_sattelites():
    pass

if __name__ == '__main__':
    gps_watching()
