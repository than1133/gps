import gpiozero

pin13 = gpiozero.Button(13)
pin19 = gpiozero.Button(19)
pin26 =  gpiozero.Button(26)

while True:
    if pin13.is_pressed:
        print("pressed from pin 13")
    if pin19.is_pressed:
        print("pressed from pin 19")
    if pin26.is_pressed:
        print("pressed from pin 26")
